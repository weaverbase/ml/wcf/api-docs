## WCF ML API

- This document URL: https://gitlab.com/weaverbase/ml/wcf/api-docs/-/blob/main/README.md

---

### Model 1

<details>
  <summary><code>GET</code> <code><b>/model1/{ACCIDENT_ISSUE_ID}</b></code> <code>(gets approval prediction)</code></summary>

##### Parameters

> | name                |  type      | data type      | description                                          |
> |---------------------|------------|----------------|------------------------------------------------------|
> | `ACCIDENT_ISSUE_ID` |  required  | string         | The specific issue unique idendifier                 |

##### Responses

> | http code     | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json;charset=UTF-8`  | JSON string                                                         |
> | `400`         | `application/json`                | `{"code":"400","message":"Bad Request"}`                            |

##### Example cURL

> ```javascript
>  curl -X GET -H "Content-Type: application/json" https://ml.weaverbase.com/dev/wcf/model1/123456
> ```

##### Response JSON format

> ```javascript
>  {
>    "INVESTIGATED_DECISION_STATUS" : "CHARACTER(1)",     // การวินิจฉัย
>    "INVESTIGATED_DECLINED_CODE"   : "CHARACTER(1)",     // สาเหตุที่ไม่มีสิทธิ
>    "INVESTIGATED_REMARK"          : "VARCHAR(200)",     // หมายเหตุ
>    "APPROVAL_FLAG"                : "CHARACTER(1)",     // การอนุมัติ
>    "APPROVAL_REMARK"              : "VARCHAR(200)"      // หมายเหตุ
>  }
> ```

</details>

---

### Model 2


<details>
  <summary><code>GET</code> <code><b>/model2/{ACCIDENT_ISSUE_ID}</b></code> <code>(gets diagnostic prediction)</code></summary>

##### Parameters

> | name                |  type      | data type      | description                                          |
> |---------------------|------------|----------------|------------------------------------------------------|
> | `ACCIDENT_ISSUE_ID` |  required  | string         | The specific issue unique idendifier                 |

##### Responses

> | http code     | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json;charset=UTF-8`  | JSON string                                                         |
> | `400`         | `application/json`                | `{"code":"400","message":"Bad Request"}`                            |

##### Example cURL

> ```javascript
>  curl -X GET -H "Content-Type: application/json" https://ml.weaverbase.com/dev/wcf/model2/123456
> ```

##### Response JSON format

> ```javascript
> {
>   "FROM_WORK_CAUSE_CODE"          : "CHARACTER(3)",     // รหัสโรคเนื่องจากการทำงาน
>   "DOCTOR_DIAGNOSTICS"            : "VARCHAR(N)",       // คำวินิจฉัยจากแพทย์ หรือผู้ที่ทำการรักษา
> }
> ```

</details>

---

### Model 3

<details>
  <summary><code>GET</code> <code><b>/model3/{TSIC_CODE}</b></code> <code>(gets fund forecast monthly)</code></summary>

##### Parameters

> | name                |  type      | data type      | description                                          |
> |---------------------|------------|----------------|------------------------------------------------------|
> | `TSIC_CODE`         |  required  | string         | The specific TSIC CODE                               |

##### Responses

> | http code     | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json;charset=UTF-8`  | JSON string                                                         |
> | `400`         | `application/json`                | `{"code":"400","message":"Bad Request"}`                            |

##### Example cURL

> ```javascript
>  curl -X GET -H "Content-Type: application/json" https://ml.weaverbase.com/dev/wcf/model3/1234
> ```

##### Response JSON format

> ```javascript
> [
>   {
>     "AMOUNT"                      : "number",           // ยอดเงิน forecast
>     "MONTH"                       : "number",           // Month (1, 2, 3, ...12)
>     "YEAR"                        : "number"            // Year (2024, 2025, ...)
>   }
> ]
> ```

</details>

---

### Model 4

<details>
  <summary><code>POST</code> <code><b>/model4</b></code> <code>(gets accident type prediction)</code></summary>

##### Request body JSON format

> ```javascript
> {
>   "ACCIDENT_CAUSE_TEXT"           : "VARCHAR(500)",     // สาเหตุที่ประสบอันตราย
>   "ACCIDENT_CASE"                 : "CHARACTER(2)",     // สาเหตุที่ประสบอันตราย
>   "ACCIDENT_INJURY_TEXT"          : "VARCHAR(500)",     // การประสบอันตราย
>   "MICROX_ACCIDENT_ORGAN_SUB_GROUP" : "VARCHAR(2)",     // ข้อมูลนำเข้ารหัสอวัยวะ
>   "MICROX_ACCIDENT_CAUSE"         : "VARCHAR(2)",       // ข้อมูลนำเข้าข้อมูลประสบอันตราย
> }
> ```

##### Responses

> | http code     | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json;charset=UTF-8`  | JSON string                                                         |
> | `400`         | `application/json`                | `{"code":"400","message":"Bad Request"}`                            |

##### Example cURL

> ```javascript
> curl -X POST -H "Content-Type: application/json" https://ml.weaverbase.com/dev/wcf/model4 \
>   -d '{
>     "ACCIDENT_CAUSE_TEXT": "...",
>     "ACCIDENT_CASE": "xx",
>     "ACCIDENT_INJURY_TEXT": "...",
>     "MICROX_ACCIDENT_ORGAN_SUB_GROUP" : "xx",
>     "MICROX_ACCIDENT_CAUSE": "xx"
>   }'
> ```

##### Response JSON format

> ```javascript
> {
>   "ACCIDENT_CAUSE"                : "CHARACTER(2)",     // รหัสสาเหตุที่ประสบอันตราย
>   "ACCIDENT_INJURY"               : "CHARACTER(2)",     // รหัสการประสบอันตราย
>   "ACCIDENT_ORGAN_SUB_GROUP"      : "CHARACTER(4)",     // รหัสอวัยวะที่ประสบอันตราย
>   "FROM_WORK_CAUSE_CODE"          : "CHARACTER(3)"      // รหัสโรคเนื่องจากการทำงาน
> }
> ```

</details>

---

### Model 5

<details>
  <summary><code>POST</code> <code><b>/model5</b></code> <code>(gets audit suggestion predictions)</code></summary>


##### Request body JSON format

> ```javascript
> {
>   "ACCIDENT_ISSUE_IDS"             : string[]
> }
> ```

##### Responses

> | http code     | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json;charset=UTF-8`  | JSON string                                                         |
> | `400`         | `application/json`                | `{"code":"400","message":"Bad Request"}`                            |

##### Example cURL

> ```javascript
> curl -X POST -H "Content-Type: application/json" https://ml.weaverbase.com/dev/wcf/model5 \
>   -d '{
>     "ACCIDENT_ISSUE_IDS": ["1000001", "1000002", "1000003"]
>   }'
> ```

##### Response JSON format

> ```javascript
> [
>   {
>     "ACCIDENT_ISSUE_ID"             : string,
>     "SUGGESTION"                    : string,             // คำแนะนำจากเจ้าหน้าที่
>     "YEAR_OF_SUGGESTION"            : number,             // ปีที่เจ้าหน้าที่ให้คำแนะนำ
>   }
> ]
> ```

</details>

